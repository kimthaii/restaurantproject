package com.example.tedtyarady.restaurant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tedtyarady.restaurant.newmodel.ResponseDataNew;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by manithnuon on 4/29/17.
 */

public class Fragment_Tab_Food extends Fragment {
  private RecyclerView recyclerView;
  private CategoryAdapter adapter;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.layout_category, container, false);
    recyclerView = (RecyclerView) view.findViewById(R.id.restaurant);
    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
//    recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));

    LinearLayoutManager manager=new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
    recyclerView.setLayoutManager(manager);

    adapter = new CategoryAdapter();
    recyclerView.setAdapter(adapter);


    Call<ResponseDataNew> call = getRetrofitManager().getPage(1);
    call.enqueue(new Callback<ResponseDataNew>() {
      @Override
      public void onResponse(Call<ResponseDataNew> call, Response<ResponseDataNew> response) {
        if (!response.isSuccessful()) return;
        ResponseDataNew data = response.body();
        if (data == null) return;
        adapter.appendToList(data.getData());
        adapter.notifyDataSetChanged();
      }


      @Override
      public void onFailure(Call<ResponseDataNew> call, Throwable t) {
      }
    });
  }

  private Service getRetrofitManager() {
    Retrofit client = new Retrofit.Builder()
            .baseUrl("http://139.59.105.74/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    return client.create(Service.class);
  }
}