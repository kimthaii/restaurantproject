package com.example.tedtyarady.restaurant;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by manithnuon on 4/29/17.
 */

public class FragmentTwoPagerAdapter extends FragmentStatePagerAdapter {
  private List<Tab_item> genres;
  public FragmentTwoPagerAdapter(FragmentManager fm, List<Tab_item> genres) {
    super(fm);
    this.genres = genres;
  }

  @Override public CharSequence getPageTitle(int position) {
    return null != genres ? genres.get(position).tabTitle : null;
  }

  @Override public Fragment getItem(int position) {
    switch (position){
      case 0 : return new Fragment_Tab_Drink();
      case 1 : return new Fragment_Tab_Food();
      default: return new Fragment_Tab_Drink();
    }
  }

  @Override public int getCount() {
    return genres.size();
  }
}
