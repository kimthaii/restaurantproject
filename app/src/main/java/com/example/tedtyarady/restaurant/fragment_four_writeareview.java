package com.example.tedtyarady.restaurant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by tedtya on 7/8/17.
 */

public class fragment_four_writeareview extends AppCompatActivity {
    private ImageView submit_button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_four_writereview);
        submit_button=(ImageView) findViewById(R.id.submit_button);


        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Data::::","Review is submitted... ");
            }
        });

    }
}
