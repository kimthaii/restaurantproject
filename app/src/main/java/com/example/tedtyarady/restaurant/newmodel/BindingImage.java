package com.example.tedtyarady.restaurant.newmodel;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * Created by tedtya on 7/7/17.
 */

public class BindingImage {

    @BindingAdapter("glideloader")
    public static void ImageLoader(ImageView view,String url){
        Glide.with(view.getContext()).load(url).into(view);
    }

}
