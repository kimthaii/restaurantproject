package com.example.tedtyarady.restaurant.newmodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ResponseDataNew{

	@SerializedName("result")
	private int result;

	@SerializedName("Message")
	private String message;

	@SerializedName("data")
	private List<DataItem> data;

	public void setResult(int result){
		this.result = result;
	}

	public int getResult(){
		return result;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setData(List<DataItem> data){
		this.data = data;
	}

	public List<DataItem> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"ResponseDataNew{" + 
			"result = '" + result + '\'' + 
			",message = '" + message + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}