package com.example.tedtyarady.restaurant;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tedtyarady.restaurant.databinding.LayoutResItemBinding;
import com.example.tedtyarady.restaurant.newmodel.DataItem;

import java.util.LinkedList;
import java.util.List;

//import com.example.tedtyarady.restaurant.databinding.LayoutResItemBinding;



public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder>{
//    public FragmentActivity fragmentActivity;
//    private RestDetailView view;
//    private View view;
//    private Context context;

    public List<DataItem> items =new LinkedList<>();

//    List<res_item> items;
//    public CategoryAdapter(List<res_item> items){
//        this.items=items;
//    }

    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_res_item,parent,false);
        LayoutResItemBinding binding= DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.layout_res_item,parent,false);
        return new CategoryAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final CategoryAdapter.ViewHolder holder, final int position) {
//        holder.res_Title.setText(food.get(position).i);


        holder.binding.setRestaurant(items.get(position));
        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(holder.binding.objImg.getContext(),fragmenttwo_next_activity_main.class);
                Intent intent=new Intent(holder.binding.objImg.getContext(),sliderPagerAdapter.class);

                i.putExtra("name",items.get(position).getName());
                i.putExtra("open",items.get(position).getOpen());
                i.putExtra("close",items.get(position).getClose());
                i.putExtra("location",items.get(position).getLocation());
                i.putExtra("latitude",items.get(position).getLat());
                i.putExtra("longititude",items.get(position).getLng());
                i.putExtra("pho1",items.get(position).getPho1());
                intent.putExtra("pho1",items.get(position).getPho1());
                intent.putExtra("pho2",items.get(position).getPho2());
                intent.putExtra("pho3",items.get(position).getPho3());


//                i.putExtra("pho1",items.get(position).getPho1());
//                i.putExtra("pho2",items.get(position).getPho2());
//                i.putExtra("pho3",items.get(position).getPho3());

//                holder.binding.objImg.getContext().startActivity(intent);
                holder.binding.objImg.getContext().startActivity(i);
            }
        });


//        Glide.with(holder.res_Image.getContext())
//                .load(items.get(position).getImgPath())
//                .into(holder.res_Image);
//        holder.res_Title.setText(items.get(position).getName());

    }

    public void appendToList(List<DataItem> list) {
        if (list == null) {
            return;
        }
        items.addAll(list);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
       LayoutResItemBinding binding;
        public ViewHolder(LayoutResItemBinding binding) {
            super(binding.getRoot());
            this.binding=binding;

        }
    }
}