package com.example.tedtyarady.restaurant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class fragmenttwo_next_activity extends Fragment implements OnMapReadyCallback {
    private MapView mapView;
    private GoogleMap gMap;
    private String name,location,open,close,latitude,longitude;
    private TextView n,l,o,c;
    private String photo1,photo2,photo3;
    private ImageView imageView;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragmenttwo_next_activity1,container,false);

//        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.image_slider);
        sliderPagerAdapter adapterView = new sliderPagerAdapter(getContext());
//        mViewPager.setAdapter(adapterView);
        n=(TextView) view.findViewById(R.id.rname);
        l=(TextView) view.findViewById(R.id.rlocation);
        o=(TextView) view.findViewById(R.id.ropen);
        c=(TextView) view.findViewById(R.id.rclose);
        imageView= (ImageView) view.findViewById(R.id.detailimg);

        name = getActivity().getIntent().getStringExtra("name");
        n.setText(name);

        location = getActivity().getIntent().getStringExtra("location");
        l.setText(location);

        open = getActivity().getIntent().getStringExtra("open");
        o.setText(open);

        close = getActivity().getIntent().getStringExtra("close");
        c.setText(close);


        photo1 =getActivity().getIntent().getStringExtra("pho1");

        Glide.with(this).load(photo1).into(imageView);
        mapView=(MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        mapView.onResume();
        return view;
    }

//    public void start(View button) {
//        if (timer != null) {
//            timer.cancel();
//        }
//        position = 0;
//        startSlider();
//    }
//
//    public void stop(View button) {
//        if (timer != null) {
//            timer.cancel();
//            timer = null;
//        }
//    }
//
//    public void startSlider() {
//        timer = new Timer();
//        timer.scheduleAtFixedRate(new TimerTask() {
//
//            public void run() {
//                // avoid exception:
//                // "Only the original thread that created a view hierarchy can touch its views"
//                runOnUiThread(new Runnable() {
//                    public void run() {
//                        imageSwitcher.setImageResource(gallery[position]);
//                        position++;
//                        if (position == gallery.length) {
//                            position = 0;
//                        }
//                    }
//                });
//            }
//
//        }, 0, DURATION);
//    }
//
//    // Stops the slider when the Activity is going into the background
//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (timer != null) {
//            timer.cancel();
//        }
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (timer != null) {
//            startSlider();
//        }
//
//    }
//



    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap=googleMap;

        latitude= getActivity().getIntent().getStringExtra("latitude");
        longitude=getActivity().getIntent().getStringExtra("longititude");

        LatLng ll=new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude));

        gMap.addMarker(new MarkerOptions().position(ll).title(name));
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ll,15));
    }



}

