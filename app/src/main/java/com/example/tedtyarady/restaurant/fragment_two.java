package com.example.tedtyarady.restaurant;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tedtya on 6/21/17.
 */

public class fragment_two extends Fragment{
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager pager;
    private FragmentTwoPagerAdapter adapter;



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_two,container,false);
        toolbar=(Toolbar) view.findViewById(R.id.toolbar);
        tabLayout=(TabLayout) view.findViewById(R.id.tab_layout);
        pager=(ViewPager) view.findViewById(R.id.category_pager);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolbar.setTitle("Category");
        adapter = new FragmentTwoPagerAdapter(getFragmentManager(),GetTab());
        pager.setAdapter(adapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.setupWithViewPager(pager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }





    private List<Tab_item> GetTab(){
        List<Tab_item> tab_item = new ArrayList<>();
        tab_item.add(new Tab_item(1,"Restaurants"));
//        tab_item.add(new Tab_item(3,"BBQ"));
        return tab_item;

    }

}
