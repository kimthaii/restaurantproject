package com.example.tedtyarady.restaurant;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class fragment_one extends Fragment {
    private View view;
    private CategoryAdapter adapter;
    private RecyclerView rv;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_one,container,false);
        rv=(RecyclerView) view.findViewById(R.id.RecyclerView_F1);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rv.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        adapter =new CategoryAdapter();
        rv.setAdapter(adapter);

    }

//
//    private List<res_item> getPopularRestaurant(){
//        List<res_item> movies = new ArrayList<>();
//        movies.add(new res_item("bbq",R.mipmap.bbq));
//        movies.add(new res_item("breakfast",R.mipmap.breakfast));
//        movies.add(new res_item("Burger",R.mipmap.burgerking));
//        movies.add(new res_item("cafe",R.mipmap.cafe));
//        movies.add(new res_item("fastfood",R.mipmap.fastfood));
//        movies.add(new res_item("breakfast",R.mipmap.breakfast));
//        movies.add(new res_item("bbq",R.mipmap.bbq));
//        return movies;
//    }

}
