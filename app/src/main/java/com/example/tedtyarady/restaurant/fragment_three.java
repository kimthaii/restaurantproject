package com.example.tedtyarady.restaurant;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tedtyarady.restaurant.newmodel.DataItem;
import com.example.tedtyarady.restaurant.newmodel.ResponseDataNew;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.LinkedList;
import java.util.List;


public class fragment_three extends Fragment implements OnMapReadyCallback{
    private View view;
    private MapView mapView;
    private ResponseDataNew response;
    GoogleMap mMap;
    public List<DataItem> items =new LinkedList<>();
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_three,container,false);
        mapView=(MapView) view.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        mapView.onResume();
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap=googleMap;
        LatLng ll=new LatLng(11.5796669,104.743725);
        Marker locationMarker;

//        for (int i = 0; i < items.size(); i++)
//        {
//            if(i==0) ////FOR ANIMATING THE CAMERA FOCUS FIRST TIME ON THE GOOGLE MAP
//            {
//                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(items.get(i).getLat()), Double.parseDouble(items.get(i).getLng()))).zoom(15).build();
//                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//            }
//
//            locationMarker = googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(items.get(i).getLat()), Double.parseDouble(items.get(i).getLng())))
////                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.YOUR_DRAWABLE))
//                    .title(items.get(i).getName())
//            );
//
//
//        }
        mMap.addMarker(new MarkerOptions().position(ll).title("khkh"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ll,10));

    }
}
