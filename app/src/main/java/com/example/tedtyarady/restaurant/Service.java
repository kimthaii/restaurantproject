package com.example.tedtyarady.restaurant;

import com.example.tedtyarady.restaurant.newmodel.ResponseDataNew;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by tedtya on 7/7/17.
 */

public interface Service {



//    @GET("v1/api.php")
//    Call<ResponseData> getID(@Query("id") int id);
//
//    @GET("v1/api.php?")
//    Call<ResponseData> getCategory(@Query("category") String category);

    @GET("v1/page.php")
    Call<ResponseDataNew> getPage(@Query("page") int page);

}
