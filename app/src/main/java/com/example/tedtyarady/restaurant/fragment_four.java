package com.example.tedtyarady.restaurant;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by tedtya on 7/2/17.
 */

public class fragment_four extends Fragment{
    public View view;
    private TextView about_us;
    private TextView privacy_policy;
    private TextView write_review;



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_four,container,false);
        about_us=(TextView) view.findViewById(R.id.about_us);
        privacy_policy=(TextView) view.findViewById(R.id.privacy_policy);
        write_review=(TextView) view.findViewById(R.id.write_review);


        next_Activity();

        return view;


    }

public void next_Activity(){

    about_us.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i=new Intent(getContext(), fragment_four_aboutus.class);
            startActivity(i);
        }
    });
    privacy_policy.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i=new Intent(getContext(),fragment_four_privacypolicy.class);
            startActivity(i);

        }
    });


    write_review.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i =new Intent(getContext(),fragment_four_writeareview.class);
            startActivity(i);
        }
    });



}



//    public View.OnClickListener btn=new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            Intent i =new Intent(getActivity(),fragment_four_aboutus.class);
//            startActivity(i);
//        }
//    };

//        @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        Button1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Intent i=new Intent(view.getContext(),fragment_four_aboutus.class);
////                startActivity(i);
//
//                Intent intent = new Intent(getActivity(), fragment_four_aboutus.class);
//                startActivity(intent);
//            }
//        });
//    }






}
