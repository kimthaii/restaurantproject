package com.example.tedtyarady.restaurant;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class AppPagerAdapter extends FragmentPagerAdapter {
    public AppPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new fragment_one();
        }
        if (position == 1) {
            return new fragment_two();
        }
        if (position == 2) {
            return new fragment_three();
        }
        else {
            return new fragment_four();
        }
    }
    @Override
    public int getCount() {
        return 4;
    }
}
