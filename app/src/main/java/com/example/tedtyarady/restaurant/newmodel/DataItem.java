package com.example.tedtyarady.restaurant.newmodel;

import com.google.gson.annotations.SerializedName;


public class DataItem{

	@SerializedName("lng")
	private String lng;

	@SerializedName("img_path")
	private String imgPath;

	@SerializedName("name")
	private String name;

	@SerializedName("pho3")
	private String pho3;

	@SerializedName("location")
	private String location;

	@SerializedName("category")
	private String category;

	@SerializedName("close")
	private String close;

	@SerializedName("open")
	private String open;

	@SerializedName("lat")
	private String lat;

	@SerializedName("pho1")
	private String pho1;

	@SerializedName("pho2")
	private String pho2;

	public void setLng(String lng){
		this.lng = lng;
	}

	public String getLng(){
		return lng;
	}

	public void setImgPath(String imgPath){
		this.imgPath = imgPath;
	}

	public String getImgPath(){
		return imgPath;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setPho3(String pho3){
		this.pho3 = pho3;
	}

	public String getPho3(){
		return pho3;
	}

	public void setLocation(String location){
		this.location = location;
	}

	public String getLocation(){
		return location;
	}

	public void setCategory(String category){
		this.category = category;
	}

	public String getCategory(){
		return category;
	}

	public void setClose(String close){
		this.close = close;
	}

	public String getClose(){
		return close;
	}

	public void setOpen(String open){
		this.open = open;
	}

	public String getOpen(){
		return open;
	}

	public void setLat(String lat){
		this.lat = lat;
	}

	public String getLat(){
		return lat;
	}

	public void setPho1(String pho1){
		this.pho1 = pho1;
	}

	public String getPho1(){
		return pho1;
	}

	public void setPho2(String pho2){
		this.pho2 = pho2;
	}

	public String getPho2(){
		return pho2;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"lng = '" + lng + '\'' + 
			",img_path = '" + imgPath + '\'' + 
			",name = '" + name + '\'' + 
			",pho3 = '" + pho3 + '\'' + 
			",location = '" + location + '\'' + 
			",category = '" + category + '\'' + 
			",close = '" + close + '\'' + 
			",open = '" + open + '\'' + 
			",lat = '" + lat + '\'' + 
			",pho1 = '" + pho1 + '\'' + 
			",pho2 = '" + pho2 + '\'' + 
			"}";
		}
}