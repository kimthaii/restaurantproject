package com.example.tedtyarady.restaurant;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tedtya on 5/17/17.
 */

public class AppFragment extends Fragment{
    private Activity activity;
    private MenuItem previtem;
    private AppPagerAdapter adapter;

    @BindView(R.id.button_navigation) BottomNavigationView bottomNavigationView;
    @BindView(R.id.viewpager) ViewPager pager;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity=(MainActivity) context;
    }
    @Override
    public void onPause() {
        super.onPause();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.button_navigation,container,false);
        bottomNavigationView=(BottomNavigationView)  view.findViewById(R.id.button_navigation);
        pager=(ViewPager) view.findViewById(R.id.viewpager);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter= new AppPagerAdapter(getFragmentManager());
        pager.setAdapter(adapter);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);


        pager.setOffscreenPageLimit(adapter.getCount()); //dont off screen page
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {

                if(previtem !=null){
                    previtem.setChecked(false);
                } else {
                    bottomNavigationView.getMenu().getItem(0).setChecked(false);
                }
                bottomNavigationView.getMenu().getItem(position).setChecked(true);
                previtem = bottomNavigationView.getMenu().getItem(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(item.getItemId() == R.id.f1){
                    pager.setCurrentItem(0);
                }else if(item.getItemId() == R.id.f2){
                    pager.setCurrentItem(1);
                }else if(item.getItemId() == R.id.f3){
                    pager.setCurrentItem(2);
                }else{
                    pager.setCurrentItem(3);
                }
                return false;
            }
        });
    }




}
