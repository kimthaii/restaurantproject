package com.example.tedtyarady.restaurant;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by tedtya on 7/17/17.
 */

public class sliderPagerAdapter extends PagerAdapter{
    Context mContext;
    private int photo1,photo2,photo3;
    private int a,b,c;
//    public List<Img_item> items =new LinkedList<>();

    sliderPagerAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
//        return sliderImagesId.length;
        return 3;
    }

    private int[] sliderImagesId = new int[]{
            R.mipmap.burgerking,R.mipmap.cafe,R.mipmap.amazon

    };

    @Override
    public boolean isViewFromObject(View v, Object obj) {
        return v == ((ImageView) obj);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int i) {
        ImageView mImageView = new ImageView(mContext);
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//        mImageView.setImageResource(sliderImagesId[i]);
        mImageView.setImageResource(sliderImagesId[i]);
        ((ViewPager) container).addView(mImageView, 0);
        return mImageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int i, Object obj) {
        ((ViewPager) container).removeView((ImageView) obj);
    }
}
