package com.example.tedtyarady.restaurant.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.tedtyarady.restaurant.MainActivity;
import com.example.tedtyarady.restaurant.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tedtya on 5/17/17.
 */

public class AppFragment extends Fragment{
    private Activity activity;
    private MenuItem previtem;
    private

    //    private BottomNavigationView bottomNavigationView;
    //    ViewPager pager;
    @BindView(R.id.button_navigation) BottomNavigationView bottomNavigationView;
    @BindView(R.id.viewpager) ViewPager viewPager;
    ViewPager pager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity=(MainActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.button_navigation,container,false);
        bottomNavigationView=(BottomNavigationView)  view.findViewById(R.id.button_navigation);
        pager=(ViewPager) view.findViewById(R.id.viewpager);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }
}
